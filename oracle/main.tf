resource "null_resource" "linux" {
  triggers = {
    #ALL_IPS = "192.168.1.164"
    always_run = "${timestamp()}"
  }

  for_each = {

    "k3sserver"   = "144.21.36.80"
    "k3sclient00" = "141.144.197.82"
    "k3sclient10" = "129.151.192.192"

  }

  connection {
    type = "ssh"
    #host     = "${self.triggers.ALL_IPS}"
    host        = each.value
    user        = "root"
    private_key = file("/home/juno/.ssh/platforma.key")
    #private_key = {
    #  content = "/home/juno/.ssh/platforma.key"
    #}

    #password = "ubuntu"
    #https    = false
    #insecure = true
    #timeout  = "20s" #"1m"
    #target_platform =  "windows"
  }

  provisioner "remote-exec" {
    inline = [
      #"ls -a",
      #"apt install nfs-common -y" #,
      "apt install open-iscsi -y",

      # "cat /etc/rancher/k3s/k3s.yaml" #,
    ]
  }
  #depends_on = [SomeResource_ID]
}

# resource "null_resource" "windows" {
#   triggers = {
#     #value = SomeResource_ID
#     always_run = "${timestamp()}"
#   }

#   connection {
#       type     = "winrm"
#       host     = "192.168.34.84"
#       user     = "localadmin"
#       password = "Computer12345678"
#       port = 5985
#       https    = false
#       insecure = true
#       timeout  = "20s" #"1m"
#       target_platform =  "windows"
#     }

#   provisioner "remote-exec" {
#     inline = [
#       "hostname" #,
#     ]
#   }
#   #depends_on = [SomeResource_ID]
# }
